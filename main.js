import app from './js/app.js';

window.addEventListener('load', () => {
   app.start();
});