let moduleActions = ( function(){

    //Varaibles privées
    let name;
    let life;
    let money;
    let awake;

    //Fonction get
    function get(){
        let getter = {
            name : moduleActions.name,
            life : moduleActions.life,
            money : moduleActions.money,
            awake : moduleActions.awake
        };
        return getter;
    }

    //Fonction init
    function init(na,li,mon,awa){
        moduleActions.name = na;
        moduleActions.life = li;
        moduleActions.money = mon;
        moduleActions.awake = awa;
    }

    //Fonction run
    function run(){
        moduleActions.life -= 1;
    }

    //Fonction fight
    function fight(){
        moduleActions.life -= 3;
    }

    //Fonction work
    function work(){
        moduleActions.life -= 1;
        moduleActions.money += 2;
    }

    //Fonction work
    function eat(){
        moduleActions.money -= 3;
        moduleActions.life += 2;

    }

    //Fonction sleep
    function sleep(){
        moduleActions.awake = false;
        setTimeout(function(){
            moduleActions.life += 1;
            moduleActions.awake = true;
        }, 10000);
    }

    //Fonction kill
    function kill(){
        moduleActions.awake = false;
        moduleActions.life = 0;
        moduleActions.money = 0;
    }

    //Fonction newLife
    function newLife(){
        moduleActions.awake = true;
        moduleActions.life = 50;
        moduleActions.money = 25;
    }

    return {
        get : get,
        init : init,
        run : run,
        fight : fight,
        work : work,
        eat : eat,
        sleep : sleep,
        kill : kill,
        newLife : newLife
    }
})();

//Exportation
export default {
    get : moduleActions.get,
    init : moduleActions.init,
    run : moduleActions.run,
    fight : moduleActions.fight,
    work : moduleActions.work,
    eat : moduleActions.eat,
    sleep : moduleActions.sleep,
    kill : moduleActions.kill,
    newLife : moduleActions.newLife
}
