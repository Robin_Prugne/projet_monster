export function displayStatus(monstre){
    let status = document.querySelector("#status");
    let nodes = status.childNodes;

    let life = document.createTextNode("life:" +monstre.life);
    let money = document.createTextNode("money:" +monstre.money);
    let awake = document.createTextNode("awake:" +monstre.awake);

    nodes[1].replaceChild(life,nodes[1].firstChild);
    nodes[2].replaceChild(money,nodes[2].firstChild);
    nodes[3].replaceChild(awake,nodes[3].firstChild);
}

export function log(message){
    let s = document.querySelector("#actionbox");
    let p = document.createElement("P");
    let t = document.createTextNode(message);

    let childNodes = s.childNodes;

    p.appendChild(t);
    s.insertBefore(p,childNodes[0]);
}

