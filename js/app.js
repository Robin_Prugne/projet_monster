import actions from "./actions.js";
import {log,displayStatus} from "./ui.js";

let moduleApp = (function() {

    //Variables privées pour dom
    let b1 = document.querySelector("#b1");
    let b2 = document.querySelector("#b2");
    let b3 = document.querySelector("#b3");
    let b4 = document.querySelector("#b4");
    let b5 = document.querySelector("#b5");
    let b6 = document.querySelector("#b6");
    let b7 = document.querySelector("#b7");
    let k = document.querySelector("#k");

    //Fonction start
    function start(){
        actions.init("Tommy",50,25,true);
        displayStatus(actions.get());

        //Fonction newLife
        let newLife = (function() {
            log(actions.get().name + " est ressuscité");
            actions.newLife();
            displayStatus(actions.get());
        });
        b1.addEventListener("click",newLife);

        //Fonction run
        let run = (function(){
            if(((actions.get().life-1) > 0) && actions.get().awake) {
                log(actions.get().name +" run");
                actions.run();
                displayStatus(actions.get());
            }
        });
        b2.addEventListener("click", run);

        //Fonction fight
        let fight = (function() {
            if(((actions.get().life-3) > 0) && actions.get().awake) {
                log(actions.get().name + " combat ");
                actions.fight();
                displayStatus(actions.get());
            }
        });
        b3.addEventListener("click", fight);

        //Fonction sleep
        let sleep = (function() {
            log(actions.get().name +" dort");
            actions.sleep();
            displayStatus(actions.get());
        });
        b4.addEventListener("click", sleep);

        //Fonction eat
        let eat = (function() {
            if(((actions.get().money-3) > 0) && actions.get().awake) {
                log(actions.get().name + " mange ");
                actions.eat();
                displayStatus(actions.get());
            }
        });
        b5.addEventListener("click", eat);

        //Fonction show
        let show = (function() {
            log("Affichage du status");

            let status = "";
            for(let i in actions.get()){
                status += i + " : " +actions.get()[i] +"\n";
            }

            alert(status);

            if(actions.get().life < 5){
                document.getElementById("monster").style.backgroundColor = "red";
            }else if (actions.get().life >= 5 && actions.get().life < 20){
                document.getElementById("monster").style.backgroundColor = "DarkOrange";
            }else if (actions.get().life >= 20 && actions.get().life < 35){
                document.getElementById("monster").style.backgroundColor = "LightSalmon";
            }else if (actions.get().life >= 35 && actions.get().life <= 50){
                document.getElementById("monster").style.backgroundColor = "Lime";
            }

            document.querySelector(".monster").style.borderWidth = (actions.get().money / 2) +"px";

        });
        b6.addEventListener("click", show);

        //Fonction work
        let work = (function(){
            if(((actions.get().life-1) > 0) && actions.get().awake) {
                log(actions.get().name + " travail");
                actions.work();
                displayStatus(actions.get());
            }
        });
        b7.addEventListener("click", work);

        //Fonction kill
        let kill = (function() {
            log(actions.get().name + " est mort");
            actions.kill();
            displayStatus(actions.get());
        });
        k.addEventListener("click",kill);

        //Fonction randomAction
        let randomAction = (function(){
            actions.get().life -= 1;
            let actionsTab = [
                actions.run,
                actions.fight,
                actions.sleep,
                actions.eat,
                actions.work
            ];

            let rdm = actionsTab[Math.floor(Math.random() * ((actionsTab.length - 1) + 1))];

            switch(rdm){
                case actions.run :
                {
                    run();
                    break;
                }
                case actions.fight :
                {
                    fight();
                    break;
                }
                case actions.sleep :
                {
                    sleep();
                    break;
                }
                case actions.eat :
                {
                    eat();
                    break;
                }
                case actions.work :
                {
                    work();
                    break;
                }
                default :
                {
                    break;
                }
            }
        });

        //Action random
        window.setInterval(randomAction, 12000);
    }

    return {
        start : start
    }
})();

export default{
    start : moduleApp.start
}

